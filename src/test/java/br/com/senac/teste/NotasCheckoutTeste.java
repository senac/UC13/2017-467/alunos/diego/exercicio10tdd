/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.teste;

import br.com.senac.model.AlunoNotaModel;
import br.com.senac.model.CheckoutAluno;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;


public class NotasCheckoutTeste {
    
    public NotasCheckoutTeste() {
    }
    
    @Test
    public void alunosNotaMaiorQue7(){
        AlunoNotaModel diego = new AlunoNotaModel ("Diego", 8, 5, 10);
        AlunoNotaModel antonio = new AlunoNotaModel("Antonio", 2, 9, 3);
        AlunoNotaModel sebastiao = new AlunoNotaModel("Sebastiao", 6, 4, 4);
        AlunoNotaModel henrique = new AlunoNotaModel("Henrique", 2, 7, 9);
        AlunoNotaModel carlos = new AlunoNotaModel("Carlos", 3, 5, 3);
        
        List<AlunoNotaModel> lista = new ArrayList<>();
        lista.add(diego);
        lista.add(antonio);
        lista.add(sebastiao);
        lista.add(henrique);
        lista.add(carlos);
        
        CheckoutAluno check = new CheckoutAluno();
        int resultado = check.checkoutAluno(lista);
        
        assertEquals(5, resultado, 0.01);
    }

}
