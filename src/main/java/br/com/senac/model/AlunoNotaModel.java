/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.senac.model;


public class AlunoNotaModel {

   private String aluno;
   private double primeiroTrimestrre;
   private double segundoTrimestrre;
   private double terceiroTrimestrre;

    public AlunoNotaModel() {
    }

    public AlunoNotaModel(String aluno, double primeiroTrimestrre, double segundoTrimestrre, double terceiroTrimestrre) {
        this.aluno = aluno;
        this.primeiroTrimestrre = primeiroTrimestrre;
        this.segundoTrimestrre = segundoTrimestrre;
        this.terceiroTrimestrre = terceiroTrimestrre;
    }


    public String getAluno() {
        return aluno;
    }

    public void setAluno(String aluno) {
        this.aluno = aluno;
    }

    public double getPrimeiroTrimestrre() {
        return primeiroTrimestrre;
    }

    public void setPrimeiroTrimestrre(double primeiroTrimestrre) {
        this.primeiroTrimestrre = primeiroTrimestrre;
    }

    public double getSegundoTrimestrre() {
        return segundoTrimestrre;
    }

    public void setSegundoTrimestrre(double segundoTrimestrre) {
        this.segundoTrimestrre = segundoTrimestrre;
    }

    public double getTerceiroTrimestrre() {
        return terceiroTrimestrre;
    }

    public void setTerceiroTrimestrre(double terceiroTrimestrre) {
        this.terceiroTrimestrre = terceiroTrimestrre;
    }

   
    
}
